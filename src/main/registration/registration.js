import "./registration.css"

const buttonReg = document.getElementById("buttonReg");

if (buttonReg) {
    buttonReg.addEventListener("click", () => servletRequestReg());
}


function servletRequestReg() {
    const payloadReg = {
        email: document.getElementById("email").value,
        login: document.getElementById("login").value,
        password: document.getElementById("password").value,
        phoneNumber: document.getElementById("phoneNumber").value,
        company: document.getElementById("companyName").value,
    };

    const errorMessage = document.getElementById('error_messages');
    errorMessage.setAttribute('style', 'display: none;')

    if (!validateLogin(payloadReg.login)) {
        errorMessage.setAttribute('style', 'display: flex;')
        errorMessage.innerText = 'Не корректный логин! Должен начинаться с большой буквы и быть не больше 15 символов'
        return;
    }

    if (!validatePassword(payloadReg.password)) {
        errorMessage.setAttribute('style', 'display: flex;');
        errorMessage.innerText = 'Некорректный пароль! Минимум 8 символов, максимум 16. Обязательно первая заглавная буква. Без знаков: !@#$%^&*?';
        return;
    }

    if (!passwordConfirm()) {
        errorMessage.setAttribute('style', 'display: flex;');
        errorMessage.innerText = 'Пароли не сопадают!';
        return;
    }

    if (!validateEmail(payloadReg.email)) {
        errorMessage.setAttribute('style', 'display: flex;');
        errorMessage.innerText = 'Некорректный имейл';
        return;
    }

    if (!validationPhoneNumber(payloadReg.phoneNumber)) {
        errorMessage.setAttribute('style', 'display: flex;');
        errorMessage.innerText = 'Некорректный номер телефона! Формат 12 цифр. +380_________';
        return;
    }

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/reg");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                document.location.href = "http://localhost:4200/index.html";
                return;
            }

            errorMessage.setAttribute('style', 'display: flex;');
            errorMessage.innerText = xhr.responseText;
        }
    };
    xhr.send(JSON.stringify(payloadReg));
}

function validationPhoneNumber(phoneNumber) {
    const re = /^\+380\d{3}\d{2}\d{2}\d{2}$/;
    return re.test(phoneNumber);
}

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateLogin(login) {
    if (login.length >= 15) {
        return false;
    }
    let character = login.charAt(0);
    return character === character.toUpperCase();
}

function validatePassword(password) {
    let re = /^.*(?=.{8,16})(?=.*[A-Z])(?=.*[a-z]).*$/;
    let character = password.charAt(0);
    return character === character.toUpperCase() && re.test(password);
}

function passwordConfirm() {
    const password = document.getElementById("password");
    const reqPassword = document.getElementById("confirmPassword");
    return password.value === reqPassword.value;
}
