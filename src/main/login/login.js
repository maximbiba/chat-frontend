import "./login.css"

var buttonAuth = document.getElementById("buttonAuth");

if (buttonAuth) {
    buttonAuth.addEventListener("click", servletRequestAuth);

}

function servletRequestAuth() {
    const payloadAuth = {
        loginOrEmail: document.getElementById("email").value,
        password: document.getElementById("password").value,
    };


    let xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/auth");
    xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE) {
            const errorMessage = document.getElementById('errorMessage');
            if (xhr.status === 200) {
                document.cookie = "login=" + xhr.responseText;
                document.location.href = "http://localhost:4200/chat.html";
                return;
            }

            errorMessage.setAttribute('style', 'display: flex;');
            errorMessage.innerText = xhr.responseText;
        }
    };
    xhr.send(JSON.stringify(payloadAuth));
}
