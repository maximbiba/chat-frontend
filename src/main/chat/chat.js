import "./chat.css"

let socket = new WebSocket("ws://localhost:8080");
let login = getCookie('login');
const sendMessageButton = document.getElementById('sendMessage');
sendMessageButton.addEventListener('click', () => sendMessage())

socket.onopen = function () {
    console.log("==================");
    console.log("Success connection");
    console.log("==================");
};

socket.onclose = function () {
    console.log("==================");
    console.log("Success close");
    console.log("==================");
};

socket.onmessage = function (event) {
    var data = JSON.parse(`${event.data}`);
    createMsgForWs(data);
};

function sendMessage() {
    const messageText = document.getElementById('message-text').value;
    const chatName = 'General';
    const messagePayload = {message: messageText, chatName, login, date: new Date().getTime()};
    socket.send(JSON.stringify(messagePayload));
    createMsgForWs({date: new Date(), login, message: messageText, chatName})
}

function createMsgForWs(data) {
    const messages = document.getElementById('messages');
    const messageDiv = document.createElement('div');
    const timeDiv = document.createElement('div');
    const loginDiv = document.createElement('div');
    const {date, login, message, chatName} = data;
    console.log(chatName);
    const time = new Date(date);
    messageDiv.innerText = message;
    timeDiv.innerText = time.toLocaleDateString();
    loginDiv.innerText = login;

    messageDiv.setAttribute('style', 'color: red;')
    timeDiv.setAttribute('style', 'color: red;')
    loginDiv.setAttribute('style', 'color: red;')

    messages.appendChild(messageDiv);
    messages.appendChild(timeDiv);
    messages.appendChild(loginDiv);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
